import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'
import {hashHistory} from 'react-router'
import configureStore from './store/configureStore'


//common style
import './static/css/common.less'
import './static/css/font.css'

// 创建 Redux 的 store 对象
const store = configureStore()

// define components
import RouteMap from './router/routeMap'

if(module.hot){
    module.hot.accept()
}

render (
    <Provider store={store}>
        <RouteMap history={hashHistory}/>
    </Provider>,
    document.getElementById('root')
)
