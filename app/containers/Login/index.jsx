import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import {bindActionCreators} from "redux";
import * as userInfoActionsFromOtherFile from "../../actions/userinfo";
import {connect} from "react-redux";
import Header from "../../components/Header";
import { hashHistory } from 'react-router'

import LoginComponent from "../../components/Login/index"

class Login extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
        this.state = {
            checking: true
        }
    }

    render() {
        return (
            <div>
                <Header title="登陆"/>
                {
                    this.state.checking
                        ? <div>{/*等待中*/}</div>
                        : <LoginComponent loginHandle={this.loginHandle.bind(this)}/>
                }
            </div>
        )
    }

    componentDidMount() {
        this.doCheck()
    }

    doCheck() {
        const userinfo = this.props.userinfo
        if (userinfo.username) {
            //已经登陆
            this.goUserPage()

        } else {
            //尚未登陆
            this.setState({
                checking: false
            })

        }
    }

    loginHandle(username) {
        //保存用户名
        const actions = this.props.userInfoActions
        let userinfo = this.props.userinfo
        userinfo.username = username
        actions.update(userinfo)

        //跳转链接
        const params = this.props.params
        const router = params.router
        if (router) {
            hashHistory.push(router)
        } else {
            this.goUserPage()
        }
    }

    goUserPage() {
        hashHistory.push('/User')
    }
}

// -------------------redux react 绑定--------------------

function mapStateToProps(state) {
    return {
        userinfo: state.userinfo
    }
}

function mapDispatchToProps(dispatch) {
    return {
        userInfoActions: bindActionCreators(userInfoActionsFromOtherFile, dispatch),
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)