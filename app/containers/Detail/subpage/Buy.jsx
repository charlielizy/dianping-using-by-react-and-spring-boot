import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import { hashHistory } from 'react-router'

import BuyAndStore from "../../../components/BuyAndStore/index"

import "./style.less"
import {bindActionCreators} from "redux";
import * as storeActionsFromOtherFile from "../../../actions/store";
import {connect} from "react-redux";

class Buy extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
        this.state = {
            isStore: false
        }
    }
    render() {
        return (
            <div>
                <BuyAndStore
                    isStore={this.state.isStore}
                    buyHandle={this.buyHandle.bind(this)}
                    storeHandle={this.storeHandle.bind(this)}/>
            </div>
        )
    }
    componentDidMount(){
        // console.log(123, this.props.store)
        // console.log(456, this.props.storeActions)
        this.checkStoreState()
    }
    //检查当前商户是否被收藏
    checkStoreState(){
        const id = this.props.id
        const store = this.props.store

        //some函数，遍历，只要有一个元素满足就可以
        store.some(item=>{
            if(item.id ===  id){
                this.setState ({
                    isStore: true
                })
                //跳出循环
                return false
            }
        })
    }
    //验证登陆
    loginCheck(){
        const id = this.props.id
        const userinfo = this.props.userinfo
        if(!userinfo.username) {
            hashHistory.push('/Login/' + encodeURIComponent('/detail/' + id))
            return false
        }
        return true
    }
    //购买事件
    buyHandle(){
        const loginFlag = this.loginCheck()
        if(!loginFlag) {
            return
        }
        //购买流程


        //跳转到用户中心页
        hashHistory.push('/User/')

    }
    //收藏事件
    storeHandle() {
        const loginFlag = this.loginCheck()
        if (!loginFlag) {
            return
        }

        const id = this.props.id
        const storeActions = this.props.storeActions

        if(this.state.isStore){
            //当前商户已经被收藏, 点击时取消收藏
            storeActions.rm({id:id})
        } else {
            //当前商户没有被收藏, 点击时收藏
            storeActions.add({id:id})
        }
        //修改状态
        this.setState ({
            isStore: !this.state.isStore
        })

    }


}

// -------------------redux react 绑定--------------------

function mapStateToProps(state) {
    return {
        userinfo: state.userinfo,
        store: state.store
    }
}

function mapDispatchToProps(dispatch) {
    return {
        storeActions: bindActionCreators(storeActionsFromOtherFile, dispatch)
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Buy)